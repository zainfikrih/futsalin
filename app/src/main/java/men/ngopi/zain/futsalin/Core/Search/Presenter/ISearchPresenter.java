package men.ngopi.zain.futsalin.Core.Search.Presenter;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.util.List;

import men.ngopi.zain.futsalin.Model.Place;

public interface ISearchPresenter {
    void onSearch(String date, String time);
    void onResponseSearch(List<String> idBooked);
    void onResponsePlace(FirestoreRecyclerOptions<Place> response);
    void onGetPlace(String city);
}
