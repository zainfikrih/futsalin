package men.ngopi.zain.futsalin.Core.Login.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import men.ngopi.zain.futsalin.Core.Login.Presenter.ILoginPresenter;
import men.ngopi.zain.futsalin.Core.Login.Presenter.LoginPresenter;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Main.View.MainActivity;
import men.ngopi.zain.futsalin.Core.Register.View.RegisterActivity;
import men.ngopi.zain.futsalin.Core.RegisterMitraActivity;

public class LoginActivity extends AppCompatActivity implements ILogin {

    private EditText loginEmailText;
    private EditText loginPasswText;
    private Button loginBtn;
    private Button loginRegBtn, loginRegMitraBtn;
    private ProgressBar loginProgress;
    private ILoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginPresenter =  new LoginPresenter(this);

        loginEmailText = (EditText) findViewById(R.id.reg_m_email);
        loginPasswText = (EditText) findViewById(R.id.reg_confirm_m_pass);
        loginBtn = (Button) findViewById(R.id.login_btn);
        loginRegBtn = (Button) findViewById(R.id.login_reg_btn);
        loginProgress = (ProgressBar) findViewById(R.id.login_progress);
        loginRegMitraBtn = (Button) findViewById(R.id.login_reg_m_btn);

        loginRegBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(regIntent);
            }
        });

        loginRegMitraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regMitraIntent = new Intent(LoginActivity.this, RegisterMitraActivity.class);
                startActivity(regMitraIntent);
            }
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String loginEmail = loginEmailText.getText().toString();
                String loginPass = loginPasswText.getText().toString();
                loginProgress.setVisibility(View.VISIBLE);
                loginPresenter.onLogin(loginEmail, loginPass);
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
        loginPresenter.onLoginCurrentUser();
    }

    private void sendToMain() {
        Intent mainIntent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onLoginSuccess(String message) {
        sendToMain();
    }

    @Override
    public void onLoginError(String message) {
        if(!message.equals("")){
            Log.i("Login Error : ", message);
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
        loginProgress.setVisibility(View.INVISIBLE);
    }
}
