package men.ngopi.zain.futsalin.Core.Admin.Presenter;

import java.util.Map;

import men.ngopi.zain.futsalin.Core.Admin.View.IAdmin;
import men.ngopi.zain.futsalin.Model.Interface.IPlace;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.Place;
import men.ngopi.zain.futsalin.Model.User;

public class AdminPresenter implements IAdminPresenter {

    private IPlace place;
    private IAdmin admin;
    private IUser user;

    public AdminPresenter(IAdmin admin){
        this.admin = admin;
        place = new Place(this);
        user = new User(this);
    }

    @Override
    public void onLogout() {
        user.logout("admin");
    }

    @Override
    public void onLogoutSuccess(String message) {
        admin.onLogout();
    }

    @Override
    public void onAddPlace(Map<String, String> dataMap, String idPlace) {
        place.onAddPlace(dataMap, idPlace);
    }

    @Override
    public void onAddSuccess(String message) {
        admin.onAddSuccess(message);
    }

    @Override
    public void onAddError(String message) {
        admin.onAddError(message);
    }
}
