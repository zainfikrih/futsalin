package men.ngopi.zain.futsalin.Core.Register.Presenter;

import android.text.TextUtils;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.User;
import men.ngopi.zain.futsalin.Core.Register.View.IRegister;

public class RegisterPresenter implements IRegisterPresenter {

    private IRegister iRegister;
    private IUser user;

    public RegisterPresenter(IRegister iRegister) {
        this.iRegister = iRegister;
        user = new User(this);
    }

    @Override
    public void onRegister(String email, String pass, String confirm_pass) {
        if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) & !TextUtils.isEmpty(confirm_pass)){
            if(pass.equals(confirm_pass)){
                user.onRegister(email, pass);
            } else {
                iRegister.onRegisterError("Confirm Password and Password Field doesn't match.");
            }
        } else {
            iRegister.onRegisterError("Fill In All Fields");
        }
    }

    @Override
    public void onRegisterSuccess(String message) {
        iRegister.onRegisterSuccess(message);
    }

    @Override
    public void onRegisterError(String message) {
        iRegister.onRegisterError(message);
    }
}
