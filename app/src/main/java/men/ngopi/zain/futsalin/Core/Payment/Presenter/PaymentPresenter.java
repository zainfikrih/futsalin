package men.ngopi.zain.futsalin.Core.Payment.Presenter;

import android.net.Uri;

import java.util.Map;

import men.ngopi.zain.futsalin.Core.Payment.View.IPayment;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Model.Order;

public class PaymentPresenter implements IPaymentPresenter {

    private IOrder order;
    private IPayment payment;

    public PaymentPresenter (IPayment payment){
        this.payment = payment;
        order = new Order(this);
    }

    @Override
    public void onPayment(String idOrder, Map<String, String> userMap) {
        order.onPayment(idOrder, userMap);
    }

    @Override
    public void onSuccess(String message) {
        payment.onSuccess(message);
    }

    @Override
    public void onError(String message) {
        if(!message.equals("")){
            payment.onError(message);
        }
    }

    @Override
    public void onDeleteOrder(String idOrder) {
        order.onTimeEnd(idOrder);
    }

    @Override
    public void onDelteRespones(String message) {
        payment.onDeleteOrder();
    }

    @Override
    public void onUploadImage(String idOrder, Uri mainImageUri) {
        if(mainImageUri != null){
            order.uploadImage(idOrder, mainImageUri);
        } else {

        }
    }

    @Override
    public void onResponeUploadImage(Uri downloadUri) {
        payment.onResponseUpload(downloadUri);
    }

    @Override
    public void onCancelOrder(String idOrder) {
        order.deleteOrder(idOrder);
    }
}
