package men.ngopi.zain.futsalin.Core.Admin.View;

public interface IAdmin {
    void onAddSuccess(String message);
    void onAddError(String message);
    void onLogout();
}
