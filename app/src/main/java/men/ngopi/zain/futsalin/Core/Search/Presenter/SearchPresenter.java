package men.ngopi.zain.futsalin.Core.Search.Presenter;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.util.List;

import men.ngopi.zain.futsalin.Core.Search.View.ISearch;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Model.Interface.IPlace;
import men.ngopi.zain.futsalin.Model.Order;
import men.ngopi.zain.futsalin.Model.Place;

public class SearchPresenter implements ISearchPresenter {

    private IPlace place;
    private ISearch search;
    private IOrder order;

    public SearchPresenter (ISearch search){
        this.search = search;
        place = new Place(this);
        order = new Order(this);
        order.onCekTimeOrder();
    }

    @Override
    public void onSearch(String date, String time) {
        place.onSearch(date, time);
    }

    @Override
    public void onResponseSearch(List<String> idBooked) {
        search.onSearchSuccess(idBooked);
    }

    @Override
    public void onResponsePlace(FirestoreRecyclerOptions<Place> response) {
        search.onPlaceSuccess(response);
    }

    @Override
    public void onGetPlace(String city) {
        place.onGetPlace(city);
    }
}
