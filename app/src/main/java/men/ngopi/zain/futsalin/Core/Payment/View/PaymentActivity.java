package men.ngopi.zain.futsalin.Core.Payment.View;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.google.firebase.storage.UploadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;
import java.util.Map;

import men.ngopi.zain.futsalin.Core.Payment.Presenter.IPaymentPresenter;
import men.ngopi.zain.futsalin.Core.Payment.Presenter.PaymentPresenter;
import men.ngopi.zain.futsalin.Core.Place.View.PlaceActivity;
import men.ngopi.zain.futsalin.Core.Search.View.SearchActivity;
import men.ngopi.zain.futsalin.Service.BroadcastService;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Main.View.MainActivity;
import men.ngopi.zain.futsalin.Core.Pemesan.PemesanActivity;

public class PaymentActivity extends AppCompatActivity implements IPayment {

    //FirebaseUser user;
//    FirebaseFirestore firebaseFirestore;
//    StorageReference storageReference;

    private String idOrder, price;
            //idUser,
    private Uri mainImageUri;
    private Uri downloadUri = null;
    private Boolean isChanged = false;

    private TextView priceTv, paymentCancel;
    private ImageView imagePay;
    private Button btnPay;
    private ProgressBar paymentProgress;
    private IPaymentPresenter paymentPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        paymentPresenter = new PaymentPresenter(this);

//        firebaseFirestore = FirebaseFirestore.getInstance();
//        user = FirebaseAuth.getInstance().getCurrentUser();
//        storageReference = FirebaseStorage.getInstance().getReference();
//        idUser = user.getUid();

        idOrder = getIntent().getExtras().getString("idOrder");
        price = getIntent().getExtras().getString("price");

        priceTv = findViewById(R.id.totalPay);
        imagePay = findViewById(R.id.imagePay);
        btnPay = findViewById(R.id.btnPay);
        paymentProgress = findViewById(R.id.paymentProgressBar);
        paymentCancel = findViewById(R.id.paymentCancel);
        priceTv.setText("Rp. "+price);

        startService(new Intent(PaymentActivity.this, BroadcastService.class));

        paymentCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentProgress.setVisibility(View.VISIBLE);
                paymentPresenter.onCancelOrder(idOrder);
            }
        });

        imagePay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(ContextCompat.checkSelfPermission(PaymentActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED){

                        ActivityCompat.requestPermissions(PaymentActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                    } else {
                        BringImagePicker();
                    }
                } else {
                    BringImagePicker();
                }
            }
        });

        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                paymentProgress.setVisibility(View.VISIBLE);
                if(mainImageUri != null){
                    if(isChanged){
                        paymentPresenter.onUploadImage(idOrder, mainImageUri);
                        unregisterReceiver(br);
//                        final StorageReference image_path = storageReference.child("pay_images").child(idOrder + ".jpg");
//                        image_path.putFile(mainImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
//                            @Override
//                            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
//                                if (!task.isSuccessful()){
//                                    throw task.getException();
//                                }
//                                return image_path.getDownloadUrl();
//                            }
//                        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
//                            @Override
//                            public void onComplete(@NonNull Task<Uri> task) {
//                                if (task.isSuccessful()){
//                                    downloadUri = task.getResult();
//                                    stopService(new Intent(PaymentActivity.this, BroadcastService.class));
//                                    storeFirestore(null, idOrder);
//                                }
//                            }
//                        });

                    } else {
                        Toast.makeText(PaymentActivity.this, "Please add image payment", Toast.LENGTH_SHORT).show();
                        paymentProgress.setVisibility(View.INVISIBLE);
                    }
                } else {
                    Toast.makeText(PaymentActivity.this, "Please add image payment", Toast.LENGTH_SHORT).show();
                    paymentProgress.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void storeFirestore(@NonNull Task<UploadTask.TaskSnapshot> task, String idOrder2) {
        if(task != null){
            //downloadUri = task.getResult().getMetadata().getReference().getDownloadUrl();
//            downloadUri = task.getResult().getStorage().getDownloadUrl();
        }else {
            //downloadUri = mainImageUri;
        }

        Map<String, String> userMap = new HashMap<>();
        userMap.put("status", "done");
        userMap.put("image", downloadUri.toString());

        paymentPresenter.onPayment(idOrder2, userMap);
    }


    private void BringImagePicker(){
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setCropShape(CropImageView.CropShape.RECTANGLE)
                .setAspectRatio(1,1)
                .setMinCropWindowSize(500,500)
                .start(PaymentActivity.this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mainImageUri = result.getUri();
                imagePay.setImageURI(mainImageUri);

                isChanged = true;
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
    }

    @Override
    protected void onPause() {
        super.onPause();
//        unregisterReceiver(br);
    }

    @Override
    protected void onStop() {
        try {
            unregisterReceiver(br);
        } catch (Exception e){

        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, BroadcastService.class));
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("Test", "Ini");
            try {
                cekTimeOrder();
            } catch (Exception e){
                Toast.makeText(PaymentActivity.this, "Ini Error Waktu Habis", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void cekTimeOrder(){
        paymentPresenter.onDeleteOrder(idOrder);
    }

    private void finishActivity(){
        PlaceActivity.getInstance().finish();
        SearchActivity.getInstance().finish();
        PemesanActivity.getInstance().finish();
    }

    @Override
    public void onSuccess(String message) {
        Toast.makeText(PaymentActivity.this, message, Toast.LENGTH_SHORT).show();

        Intent mainIntent = new Intent(PaymentActivity.this, PemesanActivity.class);
        stopService(new Intent(PaymentActivity.this, BroadcastService.class));
        finishActivity();
        paymentProgress.setVisibility(View.INVISIBLE);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onError(String message) {
        //Toast.makeText(PaymentActivity.this, " Firestore Error : " + message, Toast.LENGTH_LONG).show();
        registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
        Toast.makeText(PaymentActivity.this, "Please add image payment", Toast.LENGTH_SHORT).show();
        paymentProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDeleteOrder() {
        stopService(new Intent(PaymentActivity.this, BroadcastService.class));
        Intent myIntent = new Intent(PaymentActivity.this, PemesanActivity.class);
        startActivity(myIntent);
        finishActivity();
        finish();
    }

    @Override
    public void onResponseUpload(Uri downloadUri) {
        this.downloadUri = downloadUri;
        stopService(new Intent(PaymentActivity.this, BroadcastService.class));
        storeFirestore(null, idOrder);
    }
}
