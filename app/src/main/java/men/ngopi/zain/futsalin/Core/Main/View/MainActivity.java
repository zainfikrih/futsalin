package men.ngopi.zain.futsalin.Core.Main.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import men.ngopi.zain.futsalin.Core.Main.Presenter.IMainPresenter;
import men.ngopi.zain.futsalin.Core.Main.Presenter.MainPresenter;
import men.ngopi.zain.futsalin.Core.Admin.View.AdminActivity;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Login.View.LoginActivity;
import men.ngopi.zain.futsalin.Core.Pemesan.PemesanActivity;

public class MainActivity extends AppCompatActivity implements IMain, Animation.AnimationListener {

    private static MainActivity mainActivity;

//    private ProgressBar progressMain;

    private IMainPresenter mainPresenter;
    private Animation animFadein;
    private ImageView imageView;

    public static MainActivity getInstance(){
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainActivity = this;
        mainPresenter = new MainPresenter(this);
//        progressMain = (ProgressBar) findViewById(R.id.progressMain);
        imageView = findViewById(R.id.imageViewSplash);
//        progressMain.setVisibility(View.VISIBLE);

        animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.animator.rotation);

        animFadein.setAnimationListener(this);

        imageView.startAnimation(animFadein);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        progressMain.setVisibility(View.VISIBLE);
        mainPresenter.setMember();
    }

    private void sendToPemesan() {
        Intent pemIntent = new Intent(MainActivity.this, PemesanActivity.class);
        startActivity(pemIntent);
        finish();
    }

    private void sendToMitra() {
        Intent mitIntent = new Intent(MainActivity.this, AdminActivity.class);
        startActivity(mitIntent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(!mainPresenter.getCurrentUser()){
            sendToLogin();
        }
    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(MainActivity.this,LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }


    @Override
    public void onMainSuccess(String message) {
        if(mainPresenter.getMember().equals("1")){
            sendToPemesan();
        } else {
            sendToMitra();
        }
    }

    @Override
    public void onMainError(String message) {
        Log.i("Main Error : ", message);
        //Toast.makeText(this, "Error Main", Toast.LENGTH_SHORT).show();
//        progressMain.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
