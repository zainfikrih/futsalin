package men.ngopi.zain.futsalin.Core.Place.View;

public interface IPlace {
    void onResponseSuccess(String message);
    void onResponseError(String message);
}
