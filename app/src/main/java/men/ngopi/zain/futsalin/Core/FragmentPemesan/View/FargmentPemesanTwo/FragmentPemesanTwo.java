package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FargmentPemesanTwo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestoreException;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo.FragmentPemesanTwoPresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo.IFragmentPemesanTwoPresenter;
import men.ngopi.zain.futsalin.Service.BroadcastService;
import men.ngopi.zain.futsalin.Model.Order;
import men.ngopi.zain.futsalin.Core.Payment.View.PaymentActivity;
import men.ngopi.zain.futsalin.R;

public class FragmentPemesanTwo extends Fragment implements IFragmentPemesanTwo {

    private View view;
    private RecyclerView mOrderList;
    private FirestoreRecyclerAdapter<Order, OrderHolder> adapter;

    private IFragmentPemesanTwoPresenter fragmentPemesanTwoPresenter;

    public static FragmentPemesanTwo newInstance() {
        FragmentPemesanTwo fragment = new FragmentPemesanTwo();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();

        fragmentPemesanTwoPresenter = new FragmentPemesanTwoPresenter(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        adapter.stopListening();
        getContext().stopService(new Intent(getContext(), BroadcastService.class));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pemesan_two, container, false);

        mOrderList = (RecyclerView) view.findViewById(R.id.myrecycleviewOrder);
        mOrderList.setHasFixedSize(true);
        mOrderList.setLayoutManager(new LinearLayoutManager(getContext()));

        getContext().startService(new Intent(getContext(), BroadcastService.class));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getContext().registerReceiver(br, new IntentFilter(BroadcastService.COUNTDOWN_BR));
        fragmentPemesanTwoPresenter.onCekTimeOrder();
        fragmentPemesanTwoPresenter.onGetOrder();
        adapter.startListening();
    }

    @Override
    public void onPause() {
        super.onPause();
        getContext().unregisterReceiver(br);
    }

    @Override
    public void onStop() {
        try {
            getContext().unregisterReceiver(br);
        } catch (Exception e){

        }
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        super.onDestroy();
        adapter.stopListening();
        getContext().stopService(new Intent(getContext(), BroadcastService.class));
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("Test", "Ini");
            try {
                fragmentPemesanTwoPresenter.onCekTimeOrder();
//                Toast.makeText(getContext(), "Ini Error", Toast.LENGTH_SHORT).show();
            } catch (Exception e){
                Toast.makeText(getContext(), "Ini Error Waktu Habis", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void getOrder(FirestoreRecyclerOptions<Order> response){
        adapter = new FirestoreRecyclerAdapter<Order, OrderHolder>(response) {
            @NonNull
            @Override
            protected void onBindViewHolder(OrderHolder holder, int position, final Order model) {
                String mNama = model.getNama();
                String mNomor = "Nomor Lapangan : " + model.getNomor();
                String mKota = model.getKota();
                String mHarga = "Rp. " + model.getHarga();
                String mTanggal = model.getDate();
                String mStatus = model.getStatus();
                String mJam = model.getTime() + ":00";

                holder.uNama.setText(mNama);
                holder.uKota.setText(mKota);
                holder.uNomor.setText(mNomor);
                holder.uHarga.setText(mHarga);
                holder.uTanggal.setText(mTanggal);
                holder.uJam.setText(mJam);

                if(mStatus.equals("done")){
                    holder.uStatus.setText("Done");
                    holder.cardOrder2.setBackgroundColor(getResources().getColor(R.color.blueColor));
                }

                if(mStatus.equals("done")){
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(getContext(),"Done",Toast.LENGTH_SHORT).show();

                        }
                    });
                }

                if(mStatus.equals("not")){
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //Toast.makeText(getContext(),"Payment Pending",Toast.LENGTH_SHORT).show();
                            Intent paymentIntent = new Intent(getContext(), PaymentActivity.class);
                            paymentIntent.putExtra("idOrder",model.getId());
                            paymentIntent.putExtra("price",model.getHarga());
                            startActivity(paymentIntent);
                        }
                    });
                }
            }

            @Override
            public OrderHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.order_row, group, false);

                return new OrderHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Toast.makeText(getContext(),"Error " + e.getMessage(),Toast.LENGTH_LONG).show();
                Log.i("Error Fire" ,e.getMessage());
            }
        };

        adapter.notifyDataSetChanged();
        mOrderList.setAdapter(adapter);
    }

    @Override
    public void onResponseSuccess(FirestoreRecyclerOptions<Order> response) {
        getOrder(response);
    }

    @Override
    public void onResponseError(String message) {

    }

    public class OrderHolder extends RecyclerView.ViewHolder {
        private TextView uNama, uKota, uNomor, uHarga, uTanggal, uStatus, uJam;
        private LinearLayout cardOrder2;
        public OrderHolder(View itemView){
            super(itemView);
            uNama = itemView.findViewById(R.id.namaTempatOrder);
            uKota = itemView.findViewById(R.id.daerahOrder);
            uNomor = itemView.findViewById(R.id.nomorLapOrder);
            uHarga = itemView.findViewById(R.id.hargaOrder);
            uStatus = itemView.findViewById(R.id.statusOrder);
            uTanggal = itemView.findViewById(R.id.tglOrder);
            uJam = itemView.findViewById(R.id.jamOrder);
            cardOrder2 = itemView.findViewById(R.id.cardOrder2);
        }
    }


}
