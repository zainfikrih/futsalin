package men.ngopi.zain.futsalin.Model;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import men.ngopi.zain.futsalin.Core.Admin.Presenter.IAdminPresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanThree.IFragmentPemesanThreePresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo.IFragmentPemesanTwoPresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FargmentPemesanTwo.IFragmentPemesanTwo;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmentPemesanThree.IFragmentPemesanThree;
import men.ngopi.zain.futsalin.Core.Place.Presenter.IPlacePresenter;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Core.Login.Presenter.ILoginPresenter;
import men.ngopi.zain.futsalin.Core.Main.Presenter.IMainPresenter;
import men.ngopi.zain.futsalin.Core.Register.Presenter.IRegisterPresenter;
import men.ngopi.zain.futsalin.Core.Setup.Presenter.ISetupPresenter;

import static android.support.constraint.Constraints.TAG;

public class User implements IUser {
    private String displayName = "", password, email, userId;
    private static String member;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseFirestore firebaseFirestore;

    private IMainPresenter mainPresenter;
    private ILoginPresenter loginPresenter;
    private IRegisterPresenter registerPresenter;
    private ISetupPresenter setupPresenter;
    private IFragmentPemesanTwoPresenter fragmentPemesanTwoPresenter;
    private IFragmentPemesanThreePresenter fragmentPemesanThreePresenter;
    private IPlacePresenter placePresenter;
    private IAdminPresenter adminPresenter;

    public User(IAdminPresenter adminPresenter) {
        this.adminPresenter = adminPresenter;
        mAuth = FirebaseAuth.getInstance();
    }

    public User(IPlacePresenter placePresenter) {
        this.placePresenter = placePresenter;
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        userId = mUser.getUid();
    }

    public User(IFragmentPemesanThreePresenter fragmentPemesanThreePresenter) {
        this.fragmentPemesanThreePresenter = fragmentPemesanThreePresenter;
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        displayName = mUser.getDisplayName();
        email = mUser.getEmail();
    }

    public User(IFragmentPemesanTwoPresenter fragmentPemesanTwoPresenter) {
        this.fragmentPemesanTwoPresenter = fragmentPemesanTwoPresenter;
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        userId = mUser.getUid();
        displayName = mUser.getDisplayName();
        email = mUser.getEmail();
    }

    public User(ILoginPresenter loginPresenter) {
        this.loginPresenter = loginPresenter;
        mAuth = FirebaseAuth.getInstance();
    }

    public User(ISetupPresenter setupPresenter) {
        this.setupPresenter = setupPresenter;
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        displayName = mUser.getDisplayName();
        email = mUser.getEmail();
    }

    public User(IRegisterPresenter registerPresenter) {
        this.registerPresenter = registerPresenter;
        mAuth = FirebaseAuth.getInstance();
    }

    public User(IMainPresenter mainPresenter) {
        this.mainPresenter = mainPresenter;
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        if(mUser != null){
            displayName = mUser.getDisplayName();
            email = mUser.getEmail();
        } else {
            displayName = "";
        }
        member = "";
    }

    public User() {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        displayName = mUser.getDisplayName();
        email = mUser.getEmail();
        member = "";
    }

    public boolean getCurrentUser(){
        boolean currentUser;
        if(mUser != null){
            currentUser = true;
        } else {
            currentUser = false;
        }
        return currentUser;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPassword() {
        return password;
    }

    public String getMember() {
        return member;
    }

    public String getEmail() {
        return email;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void cekMember() {
        userId = mUser.getUid();

        firebaseFirestore.collection("Users").document(userId).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.isSuccessful()) {
                    if(task.getResult().exists()){
                        String member1 = task.getResult().getString("member");

                        // 1 -> Pemesan
                        // 2 -> Mitra
                        if(member1.equals("1")) {
                            member = "1";
                            setMember("1");
                            mainPresenter.onUserSuccess("1");
                        } else {
                            member = "2";
                            setMember("2");
                            mainPresenter.onUserSuccess("2");
                        }
                        Log.i("INI LOOO : ", member);
                    }
                }else {
                    String errorMessage = task.getException().getMessage();
                }
            }
        });

    }

    @Override
    public void onLogin(String email, String pass) {
        mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    loginPresenter.onLoginSuccess("login success");
                }else {
                    String errorMessage = task.getException().getMessage();
                    Log.i("Error Login : ", errorMessage);
                    loginPresenter.onLoginError(errorMessage);
                }
            }
        });
    }

    @Override
    public void onCurrentUser() {
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            loginPresenter.onLoginSuccess("current user login");
        } else {
            loginPresenter.onLoginError("");
        }
    }

    @Override
    public void onRegister(String email, String pass) {
        mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    Log.i("Error Tes ", "setmember");
                    setMemberPemesan();
                }else {
                    String errorMessage = task.getException().getMessage();
                    Log.i("Error Tes ", "setmember error");
                    registerPresenter.onRegisterError(errorMessage);
                }
            }
        });
    }

    public void setMemberPemesan() {
        mUser = mAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        String user_id = mUser.getUid();
        Map<String, String> userMap = new HashMap<>();
        userMap.put("member", "1");

        setMember(user_id, userMap);
    }

    public void setMember(String user_id, Map<String, String> userMap){
        firebaseFirestore.collection("Users").document(user_id).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    Log.i("Error Tes ", "setmember method");
                    registerPresenter.onRegisterSuccess("success");
                } else {
                    String errorMessage = task.getException().getMessage();
                    Log.i("Error Tes ", "setmember method error");
                    registerPresenter.onRegisterError(errorMessage);
                }
            }
        });
    }

    @Override
    public void updateProfile(String displayName, final String pass, final String newPass) {
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(displayName)
                .build();

        mUser.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()){
                    if(!pass.matches("") && !newPass.matches("")){
                        onChangePassword(pass, newPass);
                    } else {
                        setupPresenter.onSetupSuccess("Success Update Name");
                    }
                } else {
                    setupPresenter.onSetupError("Error Updated Name");
                }
            }
        });
    }

    @Override
    public String getUserId(){
        return userId;
    }

    @Override
    public void logout(String source) {
        mAuth.signOut();
        if(source.equals("admin")){
            adminPresenter.onLogoutSuccess("logout");
        } else {
            fragmentPemesanThreePresenter.onLogoutSuccess("logout");
        }
    }

    @Override
    public void onChangePassword(String pass, final String newPass) {
        AuthCredential credential = EmailAuthProvider
                .getCredential(mUser.getEmail(), pass);

        mUser.reauthenticate(credential)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mUser.updatePassword(newPass).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d(TAG, "Password updated");
                                        setupPresenter.onSetupSuccess("Success");
                                    } else {
                                        Log.d(TAG, "Error password not updated");
                                        setupPresenter.onSetupError("Password should be at least 6 characters");
                                    }
                                }
                            });
                        } else {
                            Log.d(TAG, "Error auth failed");
                            setupPresenter.onSetupError("Error to update password, old password does not match");
                        }
                    }
                });
    }
}
