package men.ngopi.zain.futsalin.Core.Register.Presenter;

public interface IRegisterPresenter {
    void onRegister(String email, String pass, String confirm_pass);
//    void setMember();
    void onRegisterSuccess(String message);
    void onRegisterError(String message);
}
