package men.ngopi.zain.futsalin.Model.Interface;

public interface IUser {
    String getEmail();
    String getDisplayName();
    String getMember();
    String getUserId();
    boolean getCurrentUser();
    void cekMember();
    void onLogin(String email, String pass);
    void onCurrentUser();
    void onRegister(String email, String pass);
    void updateProfile(String displayName, String pass, String newPass);
    void onChangePassword(String pass, String newPass);
    void logout(String source);
}
