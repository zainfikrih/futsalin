package men.ngopi.zain.futsalin.Core.Pemesan;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmantPemesanOne.FragmentPemesanOne;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmentPemesanThree.FragmentPemesanThree;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FargmentPemesanTwo.FragmentPemesanTwo;

public class PemesanActivity extends AppCompatActivity{

    private static PemesanActivity pemesanActivity;
    private Fragment selectedFragment;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(outState == null){
            getSupportFragmentManager().putFragment(outState, "myFragment", selectedFragment);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if(savedInstanceState != null){
           selectedFragment = getSupportFragmentManager().getFragment(savedInstanceState, "myFragment");
        }
    }

    public static PemesanActivity getInstance(){
        return pemesanActivity;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(selectedFragment != null){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frame_layout, selectedFragment);
            transaction.commit();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pemesan);

        pemesanActivity = this;
        selectedFragment = null;

        if (savedInstanceState != null) {
            selectedFragment = getSupportFragmentManager().getFragment(savedInstanceState, "myFragment");
        }

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.action_item1:
                        selectedFragment = FragmentPemesanOne.newInstance();
                        break;
                    case R.id.action_item2:
                        selectedFragment = FragmentPemesanTwo.newInstance();
                        break;
                    case R.id.action_item3:
                        selectedFragment = FragmentPemesanThree.newInstance();
                        break;
                }
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, selectedFragment);
                transaction.commit();
                return true;
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_layout, FragmentPemesanOne.newInstance());
        transaction.commit();
    }
}
