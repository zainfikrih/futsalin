package men.ngopi.zain.futsalin.Core;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Main.View.MainActivity;

public class RegisterMitraActivity extends AppCompatActivity {

    private EditText regEmailText, regPassText, regConfirmPassText;
    private Button regBtn, regLoginBtn;
    private ProgressBar regProgress;

    private FirebaseAuth mAuth;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseUser userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_mitra);

        mAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        regEmailText = (EditText) findViewById(R.id.reg_m_email);
        regPassText = (EditText) findViewById(R.id.reg_m_pass);
        regConfirmPassText = (EditText) findViewById(R.id.reg_confirm_m_pass);
        regBtn = (Button) findViewById(R.id.reg_m_btn);
        regLoginBtn = (Button) findViewById(R.id.reg_login_m_btn);
        regProgress = (ProgressBar) findViewById(R.id.reg_m_progress);


        regLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String email = regEmailText.getText().toString();
                String pass = regPassText.getText().toString();
                String confirm_pass = regConfirmPassText.getText().toString();

                if(!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass) & !TextUtils.isEmpty(confirm_pass)){
                    if(pass.equals(confirm_pass)){

                        regProgress.setVisibility(View.VISIBLE);

                        mAuth.createUserWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {

                                if(task.isSuccessful()){

                                    //Toast.makeText(RegisterActivity.this, "Ini Sukses", Toast.LENGTH_LONG).show();
                                    setMember();

                                }else {

                                    String errorMessage = task.getException().getMessage();
                                    Toast.makeText(RegisterMitraActivity.this, "Error : " + errorMessage, Toast.LENGTH_LONG).show();

                                }

                                regProgress.setVisibility(View.INVISIBLE);

                            }
                        });

                    } else {

                        Toast.makeText(RegisterMitraActivity.this, "Confirm Password and Password Field doesn't match.", Toast.LENGTH_LONG).show();

                    }
                }

            }
        });

    }

    private void setMember() {
        userId = mAuth.getCurrentUser();

        String user_id = userId.getUid();

        Map<String, String> userMap = new HashMap<>();
        userMap.put("member", "2");

        firebaseFirestore.collection("Users").document(user_id).set(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){

                    Toast.makeText(RegisterMitraActivity.this, "Go", Toast.LENGTH_LONG).show();
                    Intent mainIntent = new Intent(RegisterMitraActivity.this, MainActivity.class);
                    startActivity(mainIntent);
                    finish();

                } else {

                    String errorMessage = task.getException().getMessage();
                    Toast.makeText(RegisterMitraActivity.this, " Firestore Error : " + errorMessage, Toast.LENGTH_LONG).show();

                }

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            sendToMain();
        }

    }

    private void sendToMain() {

        Intent mainIntent = new Intent(RegisterMitraActivity.this, MainActivity.class);
        startActivity(mainIntent);
        finish();

    }
}
