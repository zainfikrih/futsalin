package men.ngopi.zain.futsalin.Service;

import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class BroadcastService extends Service {

    private final static String TAG = "BroadcastService";

    public static final String COUNTDOWN_BR = "men.ngopi.zain.futsalin";
    Intent bi = new Intent(COUNTDOWN_BR);

    CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        super.onCreate();

        cdt = new CountDownTimer(1800000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                bi.putExtra("countdown", millisUntilFinished);
                bi.putExtra("finish", "no");
                sendBroadcast(bi);
            }

            @Override
            public void onFinish() {
                bi.putExtra("finish", "cancel");
                sendBroadcast(bi);
            }
        };

        cdt.start();
    }

    public void onCancel(){
        cdt.cancel();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "Timer cancelled");
        super.onDestroy();
        cdt.cancel();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
