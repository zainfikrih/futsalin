package men.ngopi.zain.futsalin.Core.Place.Presenter;

import java.util.Map;

import men.ngopi.zain.futsalin.Core.Place.View.IPlace;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.Order;
import men.ngopi.zain.futsalin.Model.User;

public class PlacePresenter implements IPlacePresenter {

    private IPlace place;
    private IUser user;
    private IOrder order;
    private String userId;

    public PlacePresenter (IPlace place){
        this.place = place;
        user = new User(this);
        order = new Order(this);
        userId = user.getUserId();
    }

    @Override
    public String getUserId() {
        return userId;
    }

    @Override
    public void cekOrder(String idOrder, Map<String, String> dataMap) {
        order.cekOrder(idOrder, dataMap);
    }

    @Override
    public void onPlace(String idOrder, Map<String, String> dataMap) {
        order.onPlaceBooked(idOrder, dataMap);
    }

    @Override
    public void onResponseSuccess(String message) {
        place.onResponseSuccess(message);
    }

    @Override
    public void onResponseError(String message) {
        place.onResponseError(message);
    }
}
