package men.ngopi.zain.futsalin.Model.Interface;

import java.util.Map;

public interface IPlace {
    void onSearch(String date, String time);
    void onGetPlace(String city);
    void onAddPlace(Map<String, String> dataMap, String idPlace);
}
