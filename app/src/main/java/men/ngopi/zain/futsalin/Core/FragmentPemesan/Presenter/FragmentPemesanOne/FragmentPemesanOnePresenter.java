package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanOne;

import android.content.Intent;
import android.view.View;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmantPemesanOne.IFragmentPemesanOne;
import men.ngopi.zain.futsalin.Core.Search.View.SearchActivity;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Model.Order;

public class FragmentPemesanOnePresenter implements IFragmentPemesanOnePresenter {

    private IFragmentPemesanOne fragmentPemesanOne;
    private IOrder order;

    public FragmentPemesanOnePresenter (IFragmentPemesanOne fragmentPemesanOne){
        this.fragmentPemesanOne = fragmentPemesanOne;
        order = new Order(this);
    }

    @Override
    public void onSearch(String vCity, String uDate, String vTime, View view) {
        if(vCity != null && !uDate.equalsIgnoreCase("Choose your date ...") & vTime != null){
            Intent searchIntent = new Intent(view.getContext(), SearchActivity.class);
            searchIntent.putExtra("city", vCity);
            searchIntent.putExtra("date", uDate);
            searchIntent.putExtra("time", vTime);
            fragmentPemesanOne.onResponseSuccess("success", searchIntent);
        } else {
            fragmentPemesanOne.onResponseError("Please fill in all fields");
        }
    }

    @Override
    public void onCekTimeOrder() {
        order.onCekTimeOrder();
    }
}
