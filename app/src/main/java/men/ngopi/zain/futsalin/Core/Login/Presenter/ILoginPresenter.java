package men.ngopi.zain.futsalin.Core.Login.Presenter;

public interface ILoginPresenter {
    void onLogin(String loginEmail, String loginPass);
    void onLoginCurrentUser();
    void onLoginSuccess(String message);
    void onLoginError(String message);
}
