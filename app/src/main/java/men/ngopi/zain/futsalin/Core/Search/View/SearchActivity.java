package men.ngopi.zain.futsalin.Core.Search.View;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.List;

import men.ngopi.zain.futsalin.Core.Place.View.PlaceActivity;
import men.ngopi.zain.futsalin.Core.Search.Presenter.ISearchPresenter;
import men.ngopi.zain.futsalin.Core.Search.Presenter.SearchPresenter;
import men.ngopi.zain.futsalin.Model.Place;
import men.ngopi.zain.futsalin.R;

public class SearchActivity extends AppCompatActivity implements ISearch {

    private static SearchActivity searchActivity;

    private RecyclerView mPlaceList;
    private FirestoreRecyclerAdapter<Place, PlaceHolder> adapter;

//    private FirestoreRecyclerAdapter<Order, OrderHolder> adapter2;
    //LinearLayoutManager linearLayoutManager;
    private String city = "", date= "", time="";
    //duration="", dateStr, timeStr;
    //String[] timeFormat;
    //boolean cek = true;
    //String tempatId = "";
    private ProgressBar progressBar;
    private List<String> idBooked = new ArrayList<>();
    private ISearchPresenter searchPresenter;
    private FirestoreRecyclerOptions<Place> response;

    public static SearchActivity getInstance(){
        return searchActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchPresenter = new SearchPresenter(this);

        searchActivity = this;
        mPlaceList = (RecyclerView) findViewById(R.id.myrecycleview);
        mPlaceList.setHasFixedSize(true);
        mPlaceList.setLayoutManager(new LinearLayoutManager(this));
        progressBar = findViewById(R.id.searchProgress);

        city = getIntent().getExtras().getString("city");
        date = getIntent().getExtras().getString("date");
        time = getIntent().getExtras().getString("time");
//        duration = getIntent().getExtras().getString("duration");
//        dateStr = date.replace("/","");
//        timeStr = getIntent().getExtras().getString("sch");
//        timeFormat = timeStr.split("/");

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

//        outState.putString("timeStr", timeStr);
        outState.putString("dateStr", date);
//        outState.putString("duration", duration);
        outState.putString("time", time);
        outState.putString("date", date);
        outState.putString("city", city);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if(savedInstanceState != null){
//            timeStr = savedInstanceState.getString("timeStr");
            date = savedInstanceState.getString("dateStr");
//            duration = savedInstanceState.getString("duration");
            time = savedInstanceState.getString("time");
            date = savedInstanceState.getString("date");
            city = savedInstanceState.getString("city");
        }
    }

    private void getIdBooked(){
        searchPresenter.onSearch(date, time);
    }

    private void getPalce(){
        adapter = new FirestoreRecyclerAdapter<Place, PlaceHolder>(response) {
            @NonNull
            @Override
            protected void onBindViewHolder(PlaceHolder holder, int position, final Place model) {
                    String mKota = "Lokasi : " + model.getKota();
                    String mNomor = "Nomor Lapangan : " + model.getNomor();
                    String idBookedStr = "";
                    final String mHarga = "Rp. " + model.getHarga();

                    holder.uNama.setText(model.getNama());
                    holder.uKota.setText(mKota);
                    holder.uNomor.setText(mNomor);
                    holder.uHarga.setText(mHarga);

                for (String id:idBooked) {
                    if(model.getId().equalsIgnoreCase(id)){
                        idBookedStr = id;
                        holder.cardColor.setBackgroundColor(getResources().getColor(R.color.grey));

                        holder.itemView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Toast.makeText(SearchActivity.this,"Booked",Toast.LENGTH_SHORT).show();

                            }
                        });
                    }
                }

                if(!model.getId().equalsIgnoreCase(idBookedStr)){
                    holder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
//                            Toast.makeText(SearchActivity.this,"Klik",Toast.LENGTH_LONG).show();
                            Intent placeIntent = new Intent(SearchActivity.this, PlaceActivity.class);
                            placeIntent.putExtra("kota", model.getKota());
                            placeIntent.putExtra("nomor", model.getNomor());
                            placeIntent.putExtra("nama", model.getNama());
                            placeIntent.putExtra("harga", model.getHarga());
//                            placeIntent.putExtra("duration", duration);
                            placeIntent.putExtra("date", date);
                            placeIntent.putExtra("dateFormat", date);
                            placeIntent.putExtra("time", time);
                            placeIntent.putExtra("idTempat", model.getId());
                            startActivity(placeIntent);
                        }
                    });
                }
                progressBar.setVisibility(View.INVISIBLE);
            }

            @Override
            public PlaceHolder onCreateViewHolder(ViewGroup group, int i) {
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.place_row, group, false);

                return new PlaceHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Toast.makeText(SearchActivity.this,"Error " + e.getMessage(),Toast.LENGTH_LONG).show();
            }
        };

        adapter.notifyDataSetChanged();
        mPlaceList.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        progressBar.setVisibility(View.INVISIBLE);
        getIdBooked();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onSearchSuccess(List<String> idBooked) {
        this.idBooked = idBooked;
        searchPresenter.onGetPlace(city);
    }

    @Override
    public void onPlaceSuccess(FirestoreRecyclerOptions<Place> response) {
        this.response = response;
        getPalce();
        adapter.startListening();
    }

    @Override
    public void onSearchError(String message) {

    }

    public class PlaceHolder extends RecyclerView.ViewHolder {
        private TextView uNama, uKota, uNomor, uHarga;
        private LinearLayout cardColor;
        public PlaceHolder(View itemView){
            super(itemView);
            uNama = itemView.findViewById(R.id.namaTempatList);
            uKota = itemView.findViewById(R.id.daerahList);
            uNomor = itemView.findViewById(R.id.nomorLapList);
            uHarga = itemView.findViewById(R.id.hargaList);
            cardColor = itemView.findViewById(R.id.cardColor);
        }
    }
}
