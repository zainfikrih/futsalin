package men.ngopi.zain.futsalin.Core.Payment.View;

import android.net.Uri;

public interface IPayment {
    void onSuccess(String message);
    void onError(String message);
    void onDeleteOrder();
    void onResponseUpload(Uri downloadUri);
}
