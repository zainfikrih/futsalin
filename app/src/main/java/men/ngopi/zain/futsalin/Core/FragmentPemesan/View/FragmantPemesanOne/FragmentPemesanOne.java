package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmantPemesanOne;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.jaredrummler.materialspinner.MaterialSpinner;

import java.util.Calendar;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanOne.FragmentPemesanOnePresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanOne.IFragmentPemesanOnePresenter;
import men.ngopi.zain.futsalin.R;

public class FragmentPemesanOne extends Fragment implements IFragmentPemesanOne {

    private View view;
    private EditText homeDate;
    private android.app.DatePickerDialog dpd;
    private MaterialSpinner spinnerTime, spinnerCity;
    private Button search;

    private static String uDate =  "Choose your date ...";
    private static int uTime;
//    private static int uDuration;
    private static int uCity;

    private static String vTime;
    //private static String vDuration;
    private static String vCity;

    private static int timeInt;
    //private static int durationInt;
    //private static String sch = "";

    private IFragmentPemesanOnePresenter fragmentPemesanOnePresenter;


    public static FragmentPemesanOne newInstance() {
        FragmentPemesanOne fragment = new FragmentPemesanOne();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

//    @Override
//    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
//        String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
//        homeDate.setText(date);
//    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        //outState.putString("date", uDate);
        //outState.putInt("time", uTime);
//        outState.putInt("duration", uDuration);
        //outState.putInt("city", uCity);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null){
            //uDate = savedInstanceState.getString("date");
            //uTime = savedInstanceState.getInt("time");
//            uDuration = savedInstanceState.getInt("duration");
            //uCity = savedInstanceState.getInt("city");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pemesan_one, container, false);

        homeDate = view.findViewById(R.id.homeDate);
        spinnerTime = (MaterialSpinner) view.findViewById(R.id.homeTime);
//        spinnerDuration = (MaterialSpinner) view.findViewById(R.id.homeDuration);
        spinnerCity = (MaterialSpinner) view.findViewById(R.id.homeCity);
        search = view.findViewById(R.id.homeSearchBtn);

        homeDate.setText(uDate);
        //spinnerTime.setSelectedIndex(uTime);
//        spinnerDuration.setSelectedIndex(uDuration);
        //spinnerCity.setSelectedIndex(uCity);

        spinnerTime.setItems("00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12",
                        "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23");
        spinnerTime.setDropdownMaxHeight(500);

//        spinnerDuration.setItems("1", "2", "3", "4", "5");
//        spinnerDuration.setDropdownMaxHeight(500);

        spinnerCity.setItems("Bandung", "Jakarta", "Malang", "Surabaya", "Semarang");
        spinnerCity.setDropdownMaxHeight(500);

        spinnerTime.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                uTime = position;
                vTime = item;
                timeInt = Integer.parseInt(item);
            }
        });

//        spinnerDuration.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
//            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
//                uDuration = position;
//                vDuration = item;
//                durationInt = Integer.parseInt(item);
//            }
//        });

        spinnerCity.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener<String>() {
            @Override public void onItemSelected(MaterialSpinner view, int position, long id, String item) {
                uCity = position;
                vCity = item;
            }
        });

        homeDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                int year = now.get(Calendar.YEAR); // Initial year selection
                int month = now.get(Calendar.MONTH); // Initial month selection
                int day = now.get(Calendar.DAY_OF_MONTH); // Inital day selection

                dpd = new android.app.DatePickerDialog(view.getContext(), new android.app.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int mYear, int mMonth, int mDay) {
                        uDate = mDay+"/"+(mMonth+1)+"/"+mYear;
                        setText();
                    }
                },day,month,year);

                dpd.getDatePicker().setMinDate(System.currentTimeMillis() + 24*60*60*1000);
                dpd.show();
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        fragmentPemesanOnePresenter.onCekTimeOrder();

        if(vTime != null && vCity != null){
            spinnerCity.setText(vCity);
            spinnerTime.setText(vTime);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        fragmentPemesanOnePresenter = new FragmentPemesanOnePresenter(this);

        homeDate.setText(uDate);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentPemesanOnePresenter.onSearch(vCity, uDate, vTime, view);
            }
        });
    }

    private void setText(){
        homeDate.setText(uDate);
    }

    @Override
    public void onResponseSuccess(String message, Intent intent) {
        startActivity(intent);
    }

    @Override
    public void onResponseError(String message) {
        Toast.makeText(view.getContext(), message, Toast.LENGTH_SHORT).show();
    }
}
