package men.ngopi.zain.futsalin.Core.Main.View;

public interface IMain {
    void onMainSuccess(String message);
    void onMainError(String message);
}
