package men.ngopi.zain.futsalin.Model;

import android.support.annotation.NonNull;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import men.ngopi.zain.futsalin.Core.Admin.Presenter.IAdminPresenter;
import men.ngopi.zain.futsalin.Core.Search.Presenter.ISearchPresenter;
import men.ngopi.zain.futsalin.Model.Interface.IPlace;

public class Place implements IPlace {
    private String harga;
    private String kota;
    private String nama;
    private String nomor;
    private String id;
    private List<String> idBooked = new ArrayList<>();

    private FirebaseFirestore firebaseFirestore;
    private ISearchPresenter searchPresenter;
    private IAdminPresenter adminPresenter;

    public Place(){

    }

    public Place(IAdminPresenter adminPresenter){
        this.adminPresenter = adminPresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Place(ISearchPresenter searchPresenter) {
        this.searchPresenter = searchPresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Place(String harga, String kota, String nama, String nomor, String id) {
        this.harga = harga;
        this.kota = kota;
        this.nama = nama;
        this.nomor = nomor;
        this.id = id;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public String getKota() {
        return kota;
    }

    public String getNomor() {
        return nomor;
    }

    public String getHarga() {
        return harga;
    }

    public String getId() {
        return id;
    }

    @Override
    public void onSearch(String date, String time) {
        firebaseFirestore.collection("Order").whereEqualTo("date", date).whereEqualTo("time", time)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            return;
                        }
                        for (DocumentSnapshot doc : queryDocumentSnapshots) {
                            if (doc.get("tempat") != null) {
                                idBooked.add(doc.getString("tempat"));
                            }
                        }
                    }
                });
        searchPresenter.onResponseSearch(idBooked);
    }

    @Override
    public void onGetPlace(String city) {
        Query query = firebaseFirestore.collection("Place").whereEqualTo("kota", city);

        FirestoreRecyclerOptions<Place> response = new FirestoreRecyclerOptions.Builder<Place>()
                .setQuery(query, Place.class)
                .build();

        searchPresenter.onResponsePlace(response);
    }

    @Override
    public void onAddPlace(Map<String, String> dataMap, String idPlace) {
        firebaseFirestore.collection("Place").document(idPlace).set(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){
                    adminPresenter.onAddSuccess("Success Add");
                } else {
                    adminPresenter.onAddError("Error Add");
                }

            }
        });

    }
}
