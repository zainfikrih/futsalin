package men.ngopi.zain.futsalin.Core.Setup.View;

public interface ISetup {
    void onSetupSucces(String message);
    void onSetupError(String message);
}
