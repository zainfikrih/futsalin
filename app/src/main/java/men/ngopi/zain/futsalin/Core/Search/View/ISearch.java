package men.ngopi.zain.futsalin.Core.Search.View;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import java.util.List;

import men.ngopi.zain.futsalin.Model.Place;

public interface ISearch {
    void onSearchSuccess(List<String> idBooked);
    void onPlaceSuccess(FirestoreRecyclerOptions<Place> response);
    void onSearchError(String message);
}
