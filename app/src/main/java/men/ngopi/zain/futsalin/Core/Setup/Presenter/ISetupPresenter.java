package men.ngopi.zain.futsalin.Core.Setup.Presenter;

public interface ISetupPresenter {
    void updateProfile(String displayName, String pass, String newPass, String newPassConfirm);
    String getEmail();
    String getDisplayName();
    void onSetupSuccess(String message);
    void onSetupError(String message);
}
