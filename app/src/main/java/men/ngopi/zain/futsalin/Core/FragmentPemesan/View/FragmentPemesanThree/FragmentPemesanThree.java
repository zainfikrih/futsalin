package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmentPemesanThree;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanThree.FragmentPemesanThreePresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanThree.IFragmentPemesanThreePresenter;
import men.ngopi.zain.futsalin.Core.Login.View.LoginActivity;
import men.ngopi.zain.futsalin.Core.Pemesan.PemesanActivity;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Setup.View.SetupActivity;

public class FragmentPemesanThree extends Fragment implements IFragmentPemesanThree {

    private Button logoutBtn, pemesanProfile;
    private TextView profileName, profileEmail;
    private View view;
    private PemesanActivity pemesanActivity = new PemesanActivity();
//    private FirebaseAuth mAuth;
//    private FirebaseUser user;
    private static String nameUser, emailUser;
    private IFragmentPemesanThreePresenter fragmentPemesanThreePresenter;

    public static FragmentPemesanThree newInstance() {
        FragmentPemesanThree fragment = new FragmentPemesanThree();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mAuth = FirebaseAuth.getInstance();
//        user = mAuth.getCurrentUser();
    }

    @Override
    public void onStart() {
        super.onStart();

        fragmentPemesanThreePresenter = new FragmentPemesanThreePresenter(this);

        profileName.setText(nameUser);
        profileEmail.setText(emailUser);
    }

    @Override
    public void onResume() {
        super.onResume();

        fragmentPemesanThreePresenter.onGetProfile();
        profileName.setText(nameUser);
        profileEmail.setText(emailUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_pemesan_three, container, false);

        logoutBtn = (Button) view.findViewById(R.id.logoutPemBtn);
        pemesanProfile = view.findViewById(R.id.pemesanProfile);
        profileEmail = view.findViewById(R.id.profileEmail);
        profileName = view.findViewById(R.id.profileName);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentPemesanThreePresenter.onLogout();
            }
        });

        pemesanProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent profileIntent = new Intent(view.getContext(), SetupActivity.class);
                startActivity(profileIntent);
            }
        });

        return view;
    }

//    private void getProfile() {
//        nameUser = user.getDisplayName();
//        emailUser = user.getEmail();
//    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(view.getContext(),LoginActivity.class);
        startActivity(loginIntent);
        pemesanActivity.finish();
    }

//    public void logOut() {
//
//        mAuth.signOut();
//        sendToLogin();
//
//    }

    @Override
    public void onProfileSuccess(String name, String email) {
        nameUser = name;
        emailUser = email;
        profileName.setText(name);
        profileEmail.setText(email);
    }

    @Override
    public void onProfileError(String message) {

    }

    @Override
    public void onLogout(String message) {
        sendToLogin();
    }
}
