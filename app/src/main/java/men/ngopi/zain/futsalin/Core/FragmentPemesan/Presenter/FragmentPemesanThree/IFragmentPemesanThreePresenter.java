package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanThree;

public interface IFragmentPemesanThreePresenter {
    void onGetProfile();
    void onLogout();
    void onError(String message);
    void onLogoutSuccess(String message);
}
