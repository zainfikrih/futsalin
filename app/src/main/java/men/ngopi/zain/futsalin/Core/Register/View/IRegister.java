package men.ngopi.zain.futsalin.Core.Register.View;

public interface IRegister {
    void onRegisterSuccess(String message);
    void onRegisterError(String message);
}
