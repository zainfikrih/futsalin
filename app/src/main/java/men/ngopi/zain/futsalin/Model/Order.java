package men.ngopi.zain.futsalin.Model;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanOne.IFragmentPemesanOnePresenter;
import men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo.IFragmentPemesanTwoPresenter;
import men.ngopi.zain.futsalin.Core.Main.View.MainActivity;
import men.ngopi.zain.futsalin.Core.Payment.Presenter.IPaymentPresenter;
import men.ngopi.zain.futsalin.Core.Payment.View.PaymentActivity;
import men.ngopi.zain.futsalin.Core.Place.Presenter.IPlacePresenter;
import men.ngopi.zain.futsalin.Core.Search.Presenter.ISearchPresenter;
import men.ngopi.zain.futsalin.Core.Search.View.ISearch;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Service.BroadcastService;

public class Order implements IOrder {
    private String date, pemesan, tempat, time, id, image, status, timestamp, harga, kota, nomor, nama;

    private FirebaseFirestore firebaseFirestore;
    private StorageReference storageReference;
    private IFragmentPemesanTwoPresenter fragmentPemesanTwoPresenter;
    private IPlacePresenter placePresenter;
    private IPaymentPresenter paymentPresenter;
    private IFragmentPemesanOnePresenter fragmentPemesanOnePresenter;
    private ISearchPresenter searchPresenter;

    public Order(){

    }

    public Order(IPlacePresenter placePresenter) {
        this.placePresenter = placePresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Order(IPaymentPresenter paymentPresenter) {
        this.paymentPresenter = paymentPresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();
    }

    public Order(ISearchPresenter searchPresenter) {
        this.searchPresenter = searchPresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Order(IFragmentPemesanTwoPresenter fragmentPemesanTwoPresenter) {
        this.fragmentPemesanTwoPresenter = fragmentPemesanTwoPresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Order(IFragmentPemesanOnePresenter fragmentPemesanOnePresenter) {
        this.fragmentPemesanOnePresenter = fragmentPemesanOnePresenter;
        firebaseFirestore = FirebaseFirestore.getInstance();
    }

    public Order(String date, String pemesan, String tempat, String time, String id, String image, String status, String timestamp, String harga, String kota, String nomor, String nama) {
        this.date = date;
        this.pemesan = pemesan;
        this.tempat = tempat;
        this.time = time;
        this.id = id;
        this.image = image;
        this.status = status;
        this.timestamp = timestamp;
        this.harga = harga;
        this.kota = kota;
        this.nomor = nomor;
        this.nama = nama;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setPemesan(String pemesan) {
        this.pemesan = pemesan;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDate() {
        return date;
    }

    public String getPemesan() {
        return pemesan;
    }

    public String getTempat() {
        return tempat;
    }

    public String getTime() {
        return time;
    }

    public String getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getStatus() {
        return status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public String getHarga() {
        return harga;
    }

    public String getKota() {
        return kota;
    }

    public String getNomor() {
        return nomor;
    }

    public String getNama() {
        return nama;
    }

    @Override
    public void onGetOrder(String userId) {
        Query query = firebaseFirestore.collection("Order").orderBy("status", Query.Direction.DESCENDING)
                .orderBy("date", Query.Direction.DESCENDING)
                .orderBy("time", Query.Direction.DESCENDING)
                .whereEqualTo("pemesan", userId);

        FirestoreRecyclerOptions<Order> response = new FirestoreRecyclerOptions.Builder<Order>()
                .setQuery(query, Order.class)
                .build();

        fragmentPemesanTwoPresenter.onOrder(response);
    }

    @Override
    public void onCekTimeOrder() {
        firebaseFirestore.collection("Order")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if (e != null) {
                            return;
                        }
                        for (DocumentSnapshot doc : queryDocumentSnapshots) {
                            String timestamp = doc.getString("timestamp");
                            Long timelong = Long.parseLong(timestamp)+1800;
                            Long timenow = Timestamp.now().getSeconds();
                            if(doc.getString("status").equals("not")){
                                if(timenow >= timelong){
                                    deleteOrder(doc.getString("id"));
                                } else {
                                    Log.i("Time", "Waktu server belum habis");
                                }
                            }
                        }
                    }
                });
    }

    @Override
    public void onTimeEnd(final String idOrder) {
        try {
            firebaseFirestore.collection("Order").document(idOrder).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if(task.isSuccessful()){
                        try {
                            String timestamp = task.getResult().getString("timestamp");
                            Long timelong = Long.parseLong(timestamp)+1800;
                            Long timenow = Timestamp.now().getSeconds();
                            Log.i("Timenow",Long.toString(timelong));
                            Log.i("Timenow",Long.toString(timenow));
                            if(timenow >= timelong){
                                deleteOrder(idOrder);
                            } else {
                                Log.i("Time", "Waktu server belum habis");
                            }
                        } catch (Exception e){
                            paymentPresenter.onError("");
                        }
                    }
                }
            });
        } catch (Exception e){
            paymentPresenter.onError("");
        }
    }

    @Override
    public void deleteOrder(String idOrder){
        try {
            firebaseFirestore.collection("Order").document(idOrder).delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){
                        if(paymentPresenter != null){
                            paymentPresenter.onDelteRespones("Waktu Habis");
                        }
                        //Toast.makeText(getContext(), "Time has run out", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e){
            if(paymentPresenter != null){
                paymentPresenter.onDelteRespones("Waktu Habis");
            }
        }
    }

    @Override
    public void onPlaceBooked(String idOrder, Map<String, String> dataMap) {
        firebaseFirestore.collection("Order").document(idOrder).set(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if(task.isSuccessful()){
                    placePresenter.onResponseSuccess("Success");
                } else {
                    placePresenter.onResponseError("Error Booking Place");
                }
            }
        });
    }

    @Override
    public void onPayment(String idOrder, Map<String, String> userMap) {
        firebaseFirestore.collection("Order").document(idOrder).set(userMap, SetOptions.merge()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {
                    paymentPresenter.onSuccess("Success");
                } else {
                    String errorMessage = task.getException().getMessage();
                    paymentPresenter.onError(errorMessage);
                }
            }
        });
    }

    @Override
    public void uploadImage(String idOrder, Uri mainImageUri){
        final StorageReference image_path = storageReference.child("pay_images").child(idOrder + ".jpg");
        try {
            image_path.putFile(mainImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                @Override
                public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                    if (!task.isSuccessful()){
                        throw task.getException();
                    }
                    return image_path.getDownloadUrl();
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (task.isSuccessful()){
                        Uri downloadUri = task.getResult();
                        paymentPresenter.onResponeUploadImage(downloadUri);
                    }
                }
            });
        } catch (Exception e){
            paymentPresenter.onError("Error Upload Image");
        }
    }

    @Override
    public void cekOrder(final String idOrder, final Map<String, String> dataMap) {
        firebaseFirestore.collection("Order").document(idOrder).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if(task.getResult().exists()){
                    placePresenter.onResponseError("Already Booked First");
                } else {
                    placePresenter.onPlace(idOrder, dataMap);
                }
            }
        });
    }
}
