package men.ngopi.zain.futsalin.Core.Payment.Presenter;

import android.net.Uri;

import java.util.Map;

public interface IPaymentPresenter {
    void onPayment(String idOrder, Map<String, String> userMap);
    void onSuccess(String message);
    void onError(String message);
    void onDeleteOrder(String idOrder);
    void onDelteRespones(String message);
    void onUploadImage(String idOrder, Uri mainImageUri);
    void onResponeUploadImage(Uri downloadUri);
    void onCancelOrder(String idOrder);
}
