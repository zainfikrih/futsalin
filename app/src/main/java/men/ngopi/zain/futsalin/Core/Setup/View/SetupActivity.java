package men.ngopi.zain.futsalin.Core.Setup.View;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import men.ngopi.zain.futsalin.Core.Setup.Presenter.ISetupPresenter;
import men.ngopi.zain.futsalin.Core.Setup.Presenter.SetupPresenter;
import men.ngopi.zain.futsalin.R;

public class SetupActivity extends AppCompatActivity implements ISetup {

    //private static final String TAG = "Profile";

    private EditText name, email, pass, newPass, newPassConfirm;
    private Button setupBtn;
    private ProgressBar setupProgress;

    private ISetupPresenter setupPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        setupPresenter = new SetupPresenter(this);

        name = findViewById(R.id.setupName);
        email = findViewById(R.id.setupEmail);
        pass = findViewById(R.id.setupPass);
        newPass = findViewById(R.id.setupNewPass);
        newPassConfirm = findViewById(R.id.setupNewPassConfirm);
        setupBtn = findViewById(R.id.setupBtn);
        setupProgress = findViewById(R.id.setupProgressBar);

        if(setupPresenter.getEmail() != null){
            email.setText(setupPresenter.getEmail());
            if (setupPresenter.getDisplayName() == null){
                Toast.makeText(this, "Set your name...", Toast.LENGTH_SHORT).show();
            } else {
                name.setText(setupPresenter.getDisplayName());
            }
        } else {
            Toast.makeText(this, "User not found...", Toast.LENGTH_SHORT).show();
        }


        setupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String uName = name.getText().toString();
                final String uPass = pass.getText().toString();
                final String uPassNew = newPass.getText().toString();
                final String uPassNewConfirm = newPassConfirm.getText().toString();
                setupProgress.setVisibility(View.VISIBLE);
                if(!TextUtils.isEmpty(uName)){
                    setupPresenter.updateProfile(uName, uPass, uPassNew, uPassNewConfirm);
                } else {
                    Toast.makeText(SetupActivity.this, "Please Insert Your Name", Toast.LENGTH_SHORT).show();
                    setupProgress.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void sendToMain () {
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    public void onSetupSucces(String message) {
        sendToMain();
    }

    @Override
    public void onSetupError(String message) {
        Log.i("Error Setup : ", message);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        setupProgress.setVisibility(View.INVISIBLE);
    }
}
