package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmentPemesanThree;

public interface IFragmentPemesanThree {
    void onProfileSuccess(String name, String email);
    void onProfileError(String message);
    void onLogout(String message);
}
