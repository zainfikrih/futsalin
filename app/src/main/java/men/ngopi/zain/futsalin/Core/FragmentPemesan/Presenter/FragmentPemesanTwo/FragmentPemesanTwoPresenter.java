package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FargmentPemesanTwo.IFragmentPemesanTwo;
import men.ngopi.zain.futsalin.Model.Interface.IOrder;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.Order;
import men.ngopi.zain.futsalin.Model.User;

public class FragmentPemesanTwoPresenter implements IFragmentPemesanTwoPresenter{

    private IFragmentPemesanTwo fragmentPemesanTwo;
    private IUser user;
    private IOrder order;
    private String idUser;

    public FragmentPemesanTwoPresenter (IFragmentPemesanTwo fragmentPemesanTwo){
        this.fragmentPemesanTwo = fragmentPemesanTwo;
        user = new User(this);
        order = new Order(this);
        idUser = user.getUserId();
    }

    @Override
    public void onResponseSuccess(String message) {

    }

    @Override
    public void onResponseError(String message) {

    }

    @Override
    public void onGetOrder() {
        order.onGetOrder(idUser);
    }

    @Override
    public void onOrder(FirestoreRecyclerOptions<Order> response) {
        fragmentPemesanTwo.onResponseSuccess(response);
    }

    @Override
    public void onCekTimeOrder() {
        order.onCekTimeOrder();
    }
}
