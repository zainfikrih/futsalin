package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FargmentPemesanTwo;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import men.ngopi.zain.futsalin.Model.Order;

public interface IFragmentPemesanTwo {
    void onResponseSuccess(FirestoreRecyclerOptions<Order> response);
    void onResponseError(String message);
}
