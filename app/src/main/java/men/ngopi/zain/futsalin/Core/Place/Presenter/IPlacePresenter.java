package men.ngopi.zain.futsalin.Core.Place.Presenter;

import java.util.Map;

public interface IPlacePresenter {
    String getUserId();
    void onPlace(String idOrder, Map<String, String> dataMap);
    void onResponseSuccess(String message);
    void onResponseError(String message);
    void cekOrder(String idOrder, Map<String, String> dataMap);
}
