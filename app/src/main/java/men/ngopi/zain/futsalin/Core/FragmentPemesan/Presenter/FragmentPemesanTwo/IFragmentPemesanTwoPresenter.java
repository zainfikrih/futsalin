package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanTwo;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;

import men.ngopi.zain.futsalin.Model.Order;

public interface IFragmentPemesanTwoPresenter {
    void onResponseSuccess(String message);
    void onResponseError(String message);
    void onGetOrder();
    void onOrder(FirestoreRecyclerOptions<Order> response);
    void onCekTimeOrder();
}
