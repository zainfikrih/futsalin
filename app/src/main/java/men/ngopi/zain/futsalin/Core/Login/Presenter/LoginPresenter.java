package men.ngopi.zain.futsalin.Core.Login.Presenter;

import android.text.TextUtils;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.User;
import men.ngopi.zain.futsalin.Core.Login.View.ILogin;

public class LoginPresenter implements ILoginPresenter {

    private ILogin iLogin;
    private IUser user;

    public LoginPresenter(ILogin loginView) {
        this.iLogin = loginView;
        user = new User(this);
    }

    public void onLogin(String loginEmail, String loginPass){
        if(!TextUtils.isEmpty(loginEmail) && !TextUtils.isEmpty(loginPass)){
            user.onLogin(loginEmail, loginPass);
        } else {
            iLogin.onLoginError("Your email or password is wrong");
        }

    }

    public void onLoginCurrentUser(){
        user.onCurrentUser();

    }

    @Override
    public void onLoginSuccess(String message) {
        iLogin.onLoginSuccess(message);
    }

    @Override
    public void onLoginError(String message) {
        iLogin.onLoginError(message);
    }

}
