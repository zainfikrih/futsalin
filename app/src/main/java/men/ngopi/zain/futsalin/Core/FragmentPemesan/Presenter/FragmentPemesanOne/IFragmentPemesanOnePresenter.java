package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanOne;

import android.view.View;

public interface IFragmentPemesanOnePresenter {
    void onSearch(String vCity, String uDate, String vTime, View view);
    void onCekTimeOrder();
}
