package men.ngopi.zain.futsalin.Core.Setup.Presenter;

import android.widget.EditText;
import android.widget.TextView;

import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.User;
import men.ngopi.zain.futsalin.Core.Setup.View.ISetup;

public class SetupPresenter implements ISetupPresenter {

    private IUser iUser;
    private ISetup iSetup;
    private String email, displayName;

    public SetupPresenter(ISetup iSetup){
        this.iSetup = iSetup;
        iUser = new User(this);
        email = iUser.getEmail();
        displayName =iUser.getDisplayName();
    }

    @Override
    public void onSetupSuccess(String message) {
        iSetup.onSetupSucces("success");
    }

    @Override
    public void onSetupError(String message) {
        iSetup.onSetupError(message);
    }

    @Override
    public void updateProfile(String displayName, String pass, String newPass, String newPassConfirm) {
        if(!pass.equals("") || !newPass.equals("") || !newPassConfirm.equals("")){
            if(!pass.equals("") && !newPass.equals("") && !newPassConfirm.equals("")){
                if(newPass.equals(newPassConfirm)){
                    iUser.updateProfile(displayName, pass, newPass);
                } else {
                    iSetup.onSetupError("New Password does not match");
                }
            } else {
                iSetup.onSetupError("Please Fill in All Password Sections");
            }
        } if(pass.equals("") && newPass.equals("") && newPassConfirm.equals("")) {
            iUser.updateProfile(displayName, pass, newPass);
        }
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

}
