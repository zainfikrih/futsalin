package men.ngopi.zain.futsalin.Model.Interface;

import android.net.Uri;

import java.util.Map;

public interface IOrder {
    void onGetOrder(String userId);
    void onCekTimeOrder();
    void deleteOrder(String idOrder);
    void onPlaceBooked(String idOrder, Map<String, String> dataMap);
    void onPayment(String idOrder, Map<String, String> userMap);
    void onTimeEnd(String idOrder);
    void uploadImage(String idOrder, Uri mainImageUri);
    void cekOrder(String idOrder, Map<String, String> dataMap);
}
