package men.ngopi.zain.futsalin.Core.Register.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import men.ngopi.zain.futsalin.Core.Register.Presenter.IRegisterPresenter;
import men.ngopi.zain.futsalin.Core.Register.Presenter.RegisterPresenter;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Setup.View.SetupActivity;

public class RegisterActivity extends AppCompatActivity implements IRegister {

    private EditText regEmailText, regPassText, regConfirmPassText;
    private Button regBtn, regLoginBtn;
    private ProgressBar regProgress;
    private IRegisterPresenter registerPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        registerPresenter = new RegisterPresenter(this);

        regEmailText = (EditText) findViewById(R.id.reg_m_email);
        regPassText = (EditText) findViewById(R.id.reg_m_pass);
        regConfirmPassText = (EditText) findViewById(R.id.reg_confirm_m_pass);
        regBtn = (Button) findViewById(R.id.reg_btn);
        regLoginBtn = (Button) findViewById(R.id.reg_login_btn);
        regProgress = (ProgressBar) findViewById(R.id.reg_progress);

        regLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                regProgress.setVisibility(View.VISIBLE);
                String email = regEmailText.getText().toString();
                String pass = regPassText.getText().toString();
                String confirm_pass = regConfirmPassText.getText().toString();

                registerPresenter.onRegister(email, pass, confirm_pass);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void sendToSetup() {
        Intent mainIntent = new Intent(RegisterActivity.this, SetupActivity.class);
        startActivity(mainIntent);
        finish();
    }

//    private void sendToMain(){
//        finish();
//    }

    @Override
    public void onRegisterSuccess(String message) {
        sendToSetup();
    }

    @Override
    public void onRegisterError(String message) {
        Log.i("Register Error : ", message);
        Toast.makeText(this, "Register Error " + message, Toast.LENGTH_SHORT).show();
        regProgress.setVisibility(View.INVISIBLE);
    }
}
