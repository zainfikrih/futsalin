package men.ngopi.zain.futsalin.Core.Main.Presenter;

import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.User;
import men.ngopi.zain.futsalin.Core.Main.View.IMain;

public class MainPresenter implements IMainPresenter {

    private String email = "", displayName, member = "";

    private IMain main;
    private IUser user;

    public MainPresenter(IMain main) {
        this.main = main;
        user = new User(this);

//        try {
//            if(!user.getDisplayName().equals("")){
//                email = user.getEmail();
//                displayName = user.getDisplayName();
//            }
//        } catch (Exception e){
//            main.onMainError("Login Admin");
//        }
    }

    @Override
    public void onUserSuccess(String message) {
        cekMember(message);
    }

    @Override
    public void setMember() {
            user.cekMember();
    }

    @Override
    public void cekMember(String member1){
        if(user.getMember().equals("1")){
            member = "1";
            main.onMainSuccess("success");
        } else if(user.getMember().equals("2")) {
            member = "2";
            main.onMainSuccess("success");
        } else {
            main.onMainError("Error Main Set Member");
        }
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getDisplayName() {
        return displayName;
    }

    @Override
    public String getMember() {
        return member;
    }

    @Override
    public boolean getCurrentUser() {
        return user.getCurrentUser();
    }
}
