package men.ngopi.zain.futsalin.Core.Admin.Presenter;

import java.util.Map;

public interface IAdminPresenter {
    void onLogout();
    void onLogoutSuccess(String message);
    void onAddPlace(Map<String, String> dataMap, String idPlace);
    void onAddSuccess(String message);
    void onAddError(String message);
}