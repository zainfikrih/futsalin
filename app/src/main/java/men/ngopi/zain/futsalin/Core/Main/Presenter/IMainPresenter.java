package men.ngopi.zain.futsalin.Core.Main.Presenter;

public interface IMainPresenter {
    void setMember();
    void cekMember(String member);
    String getEmail();
    String getDisplayName();
    String getMember();
    boolean getCurrentUser();
    void onUserSuccess(String message);
}
