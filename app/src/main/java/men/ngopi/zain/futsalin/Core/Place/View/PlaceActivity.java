 package men.ngopi.zain.futsalin.Core.Place.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.Timestamp;

import java.util.HashMap;
import java.util.Map;

import men.ngopi.zain.futsalin.Core.Payment.View.PaymentActivity;
import men.ngopi.zain.futsalin.Core.Place.Presenter.IPlacePresenter;
import men.ngopi.zain.futsalin.Core.Place.Presenter.PlacePresenter;
import men.ngopi.zain.futsalin.Core.Search.View.SearchActivity;
import men.ngopi.zain.futsalin.R;

 public class PlaceActivity extends AppCompatActivity implements IPlace {

     private static PlaceActivity placeActivity;

     private TextView nama, location, number, date, time, total2;
     private String rentStr, durationStr, namaStr, kotaStr, nomorStr, dateStr, dateFormat, idTempat, timeFormat, idOrder;
     private Button placeBtn;
     private ProgressBar placeProgress;
     private IPlacePresenter placePresenter;

     public static PlaceActivity getInstance(){
        return placeActivity;
     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place);

        placePresenter = new PlacePresenter(this);

        placeActivity = this;

        nama = findViewById(R.id.placeNama);
        location = findViewById(R.id.placeLokasi);
        number = findViewById(R.id.placeNomor);
        date = findViewById(R.id.placeDate);
        time = findViewById(R.id.placeTime);
        total2 = findViewById(R.id.placeTotal2);
        placeBtn = findViewById(R.id.placeBtnOrder);
        placeProgress = findViewById(R.id.placeProgressBar);

        rentStr = getIntent().getExtras().getString("harga");
//        durationStr = getIntent().getExtras().getString("duration");
        namaStr = getIntent().getExtras().getString("nama");
        kotaStr = getIntent().getExtras().getString("kota");
        nomorStr = getIntent().getExtras().getString("nomor");
        dateStr = getIntent().getExtras().getString("date");
        dateFormat = dateStr.replace("/","");
        idTempat = getIntent().getExtras().getString("idTempat");
        timeFormat = getIntent().getExtras().getString("time");
        idOrder = idTempat+dateFormat+timeFormat;

//        int rentInt = Integer.parseInt(rentStr);
//        int durationInt = Integer.parseInt(durationStr);
//        int totalInt = rentInt*durationInt;
//        String totalStr = String.valueOf(totalInt);

        nama.setText(namaStr);
        location.setText(kotaStr);
        number.setText("Lapangan "+nomorStr);
        date.setText(dateStr);
        time.setText(timeFormat+":00 WIB");
        total2.setText("Rp. "+rentStr);
//        duration.setText(durationStr);
//        total.setText(totalStr);
//        total2.setText(totalStr);

        final Map<String, String> dataMap = new HashMap<>();
        dataMap.put("pemesan", placePresenter.getUserId());
        dataMap.put("tempat", idTempat);
        dataMap.put("time", timeFormat);
        dataMap.put("date", dateStr);
        dataMap.put("id", idOrder);
        dataMap.put("harga", rentStr);
        dataMap.put("nomor", nomorStr);
        dataMap.put("nama", namaStr);
        dataMap.put("kota", kotaStr);
        dataMap.put("status", "not");
        dataMap.put("timestamp", Long.toString(Timestamp.now().getSeconds()));


        placeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeProgress.setVisibility(View.VISIBLE);
                placePresenter.cekOrder(idOrder, dataMap);
            }
        });
    }

     private void finishActivity(){
         SearchActivity.getInstance().finish();
     }

     @Override
     public void onResponseSuccess(String message) {
         //Toast.makeText(PlaceActivity.this,message, Toast.LENGTH_LONG).show();
         Intent paymentIntent = new Intent(PlaceActivity.this, PaymentActivity.class);
         paymentIntent.putExtra("idOrder",idOrder);
         paymentIntent.putExtra("price",rentStr);
         placeProgress.setVisibility(View.INVISIBLE);
         finishActivity();
         startActivity(paymentIntent);
         finish();
     }

     @Override
     public void onResponseError(String message) {
         Toast.makeText(PlaceActivity.this,message, Toast.LENGTH_LONG).show();
         placeProgress.setVisibility(View.INVISIBLE);
     }
}
