package men.ngopi.zain.futsalin.Core.Admin.View;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

import men.ngopi.zain.futsalin.Core.Admin.Presenter.IAdminPresenter;
import men.ngopi.zain.futsalin.Core.Admin.Presenter.AdminPresenter;
import men.ngopi.zain.futsalin.R;
import men.ngopi.zain.futsalin.Core.Login.View.LoginActivity;

public class AdminActivity extends AppCompatActivity implements IAdmin {

//    private FirebaseAuth mAuth;
//    private FirebaseUser user;
//    private FirebaseFirestore firebaseFirestore;
    private Button logoutBtn, addBtn;
    private EditText nama, nomor, harga;
    private Spinner kota;
    private ProgressBar progBar;
    private IAdminPresenter adminPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        adminPresenter = new AdminPresenter(this);

//        mAuth = FirebaseAuth.getInstance();
//        firebaseFirestore = FirebaseFirestore.getInstance();
//        user = mAuth.getCurrentUser();

        kota = findViewById(R.id.spinnerKota);
        nama = findViewById(R.id.namaTempat);
        nomor = findViewById(R.id.nomorLap);
        harga = findViewById(R.id.harga);
        progBar = findViewById(R.id.progBar);
        addBtn = findViewById(R.id.addBtn);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.kota, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        kota.setAdapter(adapter);

        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //String userId = user.getUid();

                String kotaStr = kota.getSelectedItem().toString();
                String namaStr = nama.getText().toString();
                String nomorStr = nomor.getText().toString();
                String hargaStr = harga.getText().toString();

                Map<String, String> dataMap = new HashMap<>();
                dataMap.put("kota", kotaStr);
                dataMap.put("nama", namaStr);
                dataMap.put("nomor", nomorStr);
                dataMap.put("harga", hargaStr);
                dataMap.put("id", namaStr    +nomorStr);

                if(!TextUtils.isEmpty(kotaStr) && !TextUtils.isEmpty(namaStr) & !TextUtils.isEmpty(nomorStr) & !TextUtils.isEmpty(hargaStr)){

                    progBar.setVisibility(View.VISIBLE);

                    adminPresenter.onAddPlace(dataMap, namaStr+nomorStr);
//                    firebaseFirestore.collection("Place").document(userId+namaStr+nomorStr).set(dataMap).addOnCompleteListener(new OnCompleteListener<Void>() {
//                        @Override
//                        public void onComplete(@NonNull Task<Void> task) {
//
//                            if(task.isSuccessful()){
//                                Toast.makeText(AdminActivity.this,"Berhasil Ditambahkan", Toast.LENGTH_LONG).show();
//                            } else {
//                                Toast.makeText(AdminActivity.this,"Gagal Ditambahkan", Toast.LENGTH_LONG).show();
//                            }
//
//                            progBar.setVisibility(View.INVISIBLE);
//
//                        }
//                    });

                }
            }
        });

        logoutBtn = (Button) findViewById(R.id.logoutMitBtn);

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logOut();
            }
        });
    }

    private void sendToLogin() {
        Intent loginIntent = new Intent(AdminActivity.this,LoginActivity.class);
        startActivity(loginIntent);
        finish();
    }

    private void logOut() {

        //mAuth.signOut();
        adminPresenter.onLogout();


    }

    @Override
    public void onLogout() {
        sendToLogin();
    }

    @Override
    public void onAddSuccess(String message) {
        Toast.makeText(AdminActivity.this,"Berhasil Ditambahkan", Toast.LENGTH_LONG).show();
        progBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAddError(String message) {
        Toast.makeText(AdminActivity.this,"Gagal Ditambahkan", Toast.LENGTH_LONG).show();
        progBar.setVisibility(View.INVISIBLE);
    }
}
