package men.ngopi.zain.futsalin.Core.FragmentPemesan.Presenter.FragmentPemesanThree;

import men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmentPemesanThree.IFragmentPemesanThree;
import men.ngopi.zain.futsalin.Model.Interface.IUser;
import men.ngopi.zain.futsalin.Model.User;

public class FragmentPemesanThreePresenter implements IFragmentPemesanThreePresenter {

    private IFragmentPemesanThree fragmentPemesanThree;
    private IUser user;
    private String name, email;

    public FragmentPemesanThreePresenter (IFragmentPemesanThree fragmentPemesanThree){
        this.fragmentPemesanThree = fragmentPemesanThree;
        user = new User(this);

    }

    @Override
    public void onGetProfile() {
        name = user.getDisplayName();
        email = user.getEmail();
        if(name != null){
            fragmentPemesanThree.onProfileSuccess(name, email);
        } else {
            fragmentPemesanThree.onProfileError("No User Current");
        }
    }

    @Override
    public void onLogout() {
        user.logout("user");
    }

    @Override
    public void onError(String message) {

    }

    @Override
    public void onLogoutSuccess(String message) {
        fragmentPemesanThree.onLogout(message);
    }
}
