package men.ngopi.zain.futsalin.Core.FragmentPemesan.View.FragmantPemesanOne;

import android.content.Intent;

public interface IFragmentPemesanOne {
    void onResponseSuccess(String message, Intent intent);
    void onResponseError(String message);
}
