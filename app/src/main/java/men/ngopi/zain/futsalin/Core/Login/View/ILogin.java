package men.ngopi.zain.futsalin.Core.Login.View;

public interface ILogin {
    void onLoginSuccess(String message);
    void onLoginError(String message);
}
